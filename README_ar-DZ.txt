﻿التدقيق النحوي العربي
1- تدقيق التاء المربوطة
2- تدقيق تكرار الكلمات
3- تدقيق الأخطاء الشائعة
Arabic grammar checker for OpenOffice.org

(developed by the Lightproof grammar checker extension generator,
see http://launchpad.net/lightproof)

2011 (c) Taha Zerrouk, license: GNU LGPL
