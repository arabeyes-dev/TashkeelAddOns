dist: en_US hu_HU ru_RU
	rm -f META-INF/* dialog/* pythonpath/*
	zip -r lightproof-`cat VERSION`.zip *_*.oxt META-INF dialog pythonpath \
	VERSION* data NEWS ChangeLog* TODO THANKS *_py.py Dialog.py Compile.py \
	Makefile README* make.py Linguistic_xcu.py description_xml.py doc

hu_HU:
	make clean
	python make.py -v `grep hu_HU VERSIONS | cut -d ' ' -f 2` -d data hu_HU
	zip -r lightproof-hu_HU-`grep hu_HU VERSIONS | cut -d ' ' -f 2`.oxt \
	META-INF pythonpath dialog Lightproof.py *xcu *xml README_hu_HU ChangeLog_hu_HU

en_US:
	make clean
	python make.py -v `grep en_US VERSIONS | cut -d ' ' -f 2` -d data en_US
	zip -r lightproof-en_US-`grep en_US VERSIONS | cut -d ' ' -f 2`.oxt \
	META-INF pythonpath dialog Lightproof.py *xcu *xml README_en_US

ru_RU:
	make clean
	python make.py -v `grep ru_RU VERSIONS | cut -d ' ' -f 2` -d data ru_RU
	zip -r lightproof-ru_RU-`grep ru_RU VERSIONS | cut -d ' ' -f 2`.oxt \
	META-INF pythonpath dialog Lightproof.py *xcu *xml README_ru_RU

clean:
	@rm -f pythonpath/* dialog/*
