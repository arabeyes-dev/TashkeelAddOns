#generate arabic tashkeel

del /Q META-INF\*
del /Q pythonpath\*
del /Q dialog\*
del /Q *xcu
del /Q *xml
# README_ar_DZ.txt
python make.py -v 0.1 -d data ar_DZ
mkdir tashkeel-ar_DZ-0.1
mkdir tashkeel-ar_DZ-0.1\META-INF
mkdir tashkeel-ar_DZ-0.1\pythonpath
mkdir tashkeel-ar_DZ-0.1\dialog

copy META-INF  tashkeel-ar_DZ-0.1\META-INF
copy pythonpath  tashkeel-ar_DZ-0.1\pythonpath
copy dialog  tashkeel-ar_DZ-0.1\dialog
copy tashkeel.py   tashkeel-ar_DZ-0.1
copy *.xcu  tashkeel-ar_DZ-0.1
copy *.xml  tashkeel-ar_DZ-0.1
copy README_ar_DZ.txt  tashkeel-ar_DZ-0.1
