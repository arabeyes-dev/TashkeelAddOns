﻿مساعد التشكيل العربية
Arabic Tashkeel Assistant for OpenOffice.org

(developed by the Lightproof grammar checker extension generator,
see http://launchpad.net/lightproof)

2011 (c) Taha Zerrouk, license: GNU LGPL
